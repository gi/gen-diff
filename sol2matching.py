#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021 Diego Rubert
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software. If not, see <https://www.gnu.org/licenses/>


#TODO: support for Gurobi

import xml.etree.ElementTree as ET
from argparse import ArgumentParser, FileType
from util import readGenomesMap, parseSolMatching, readAndTranslateSimFile, readGeneNumberMap, FAMILY_FREE_ID
from os import path
from sys import stdout, stderr


def getGeneNumber(genome, geneidmap, gene_ext_tag):
    """Finds the gene number from a gene extremity tag used in the ILP,
       needs the unique gene ID map (from unique to original ID) and
       the genome.
    """
    num,label,ext = gene_ext_tag.split('_') # gene number, genome and extremity type
    gname = genome['name']
    num = geneidmap[gname][int(num)]
    return num


def main(): # so variables are NOT global
    description = 'Reads a similarity (.sim) file, a (cplex) solution (.sol) file and other auxiliary files and prints the edges in the similarity file corresponding to the matching in the solution. Edges are printed using the gene identifiers, not internal numeric IDs.'
    parser = ArgumentParser(description=description)
    
    parser.add_argument('-m', '--genome-map', metavar='genome_map.cfg', required=True, type=FileType('r'),
                        help='File with genome info (given by FFGC).')
    parser.add_argument('-i', '--sim', metavar='file.sim', required=True, type=FileType('r'),
                        help='Marker similarities file (given by FFGC).')
    parser.add_argument('-s', '--sol', metavar='file.sol', required=True, type=FileType('r'),
                        help='Solution file (given by CPLEX), genome names are taken from the guess in --sim option.') #TODO: add support for gurobi, with a --mst option conflicting with this one
    parser.add_argument('-g', '--geneid-map', metavar='file.gidmap', required=True, type=FileType('r'),
                        help='File with gene IDs mapping (given by ffgc_to_ilp.py), that maps gene numbers in the .sim (not the original ones, as ffgc_to_ilp.py translates from active genes order to global genes order) to the unique numbers used in the ILP file. This file is also used to guess genome names Gx (string after 1st \'>\') and Gy (string after 2nd \'>\'), where Gx and Gy are the genome names as in genome_map.cfg and should match to the name of other files (e.g. Gx_Gy.sim).')
    parser.add_argument('-o', '--sim-out', metavar='matching.sim', required=False, type=FileType('w'), default=stdout,
                        help='Output file in .sim format with matching edges (default: stdout).')
 
    args = parser.parse_args()

    print("Reading genome map.", file=stderr)
    genomes = readGenomesMap(args.genome_map)
    print("Reading solution file.", file=stderr)
    matching = parseSolMatching(args.sol)
    print("Reading gene ID map.", file=stderr)
    geneidmap, gname1, gname2 = readGeneNumberMap(args.geneid_map)

    geneidmap_rev = { genome: {v:k for k,v in genes.items() } for genome,genes in geneidmap.items() }
    matching = { ( getGeneNumber(genomes[gname1], geneidmap_rev, e[0]),
                   getGeneNumber(genomes[gname2], geneidmap_rev, e[1])
                 ) for e in matching }

    print("Reading similarities.", file=stderr)
    edges = readAndTranslateSimFile(genomes[gname1], genomes[gname2], args.sim, FAMILY_FREE_ID, 0, keep_chr_data=True)
    edges = [ e for e in edges if (e['g1'], e['g2']) in matching ]
    
    edges_locus = list()
    for e in edges:
        gene1 = genomes[gname1]['genes'][e['g1']]
        gene2 = genomes[gname2]['genes'][e['g2']]
        w = e['weight']

        new_e = (e['chr1'], gene1['id'], e['chr2'], gene2['id'], str(e['rel_orient']), str(w))
        edges_locus.append(new_e)
            
    print("Writing output.", file=stderr)
    for e in edges_locus:
        print('\t'.join(e), file=args.sim_out)
    
    exit(0)

if __name__ == "__main__":
    main()

