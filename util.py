# Auxiliary functions needed by different scripts of the DCJIdDup ILP processing,
# as well as for adapting Mcd/mcdmm+ to the UniMoG format.

from re import compile
from configparser import ConfigParser
from collections import OrderedDict
from string import ascii_uppercase
from os import path
from sys import stderr
import xml.etree.ElementTree as ET


CHR_CIRCULAR = ')'
CHR_LINEAR = '|'
ORIENT_POSITIVE = '+'
ORIENT_NEGATIVE = '-'
EXTREMITY_TAIL = 't' # must be a single char
EXTREMITY_HEAD = 'h' # must be a single char
EXTREMITY_TELOMERE = '$' # must be a single char
#all telomeres are equivalent concerning the distance
TELOMERE_ID = -1

FAMILY_FREE = 'ffdcj'
FAMILY_FREE_ID = 'ffdcjindel'
UNW_FAMILY_FREE_ID = 'unwffdcjindel'

# Here (?:...) makes a non-capturing group, because we CAN match pipe
# | inside a character class [] but we CAN'T match end of line $ ($
# looses its special meaning, representing then a literal $, there is
# no way to change that).
PATTERN_LOCUS = compile('^.*\|([0-9]+:[0-9]+)(?:$|\|)')
PATTERN_LOCUS_TAG = compile('.*\|locus\|([^\|]+)(?:$|\|)')
PATTERN_CHR = compile('.*\|chromosome\|([^\|]+)(?:$|\|)')
PATTERN_STRAND = compile('.*\|strand\|([+-])(?:$|\|)')

FASTA_FILE_KEY = 'fasta_file'
ACTIVE_GENES_KEY = 'active_genes'


#TODO: protect for parallel execution
class Simple_Id_Generator:
    last = 0
    def get_new(self):
        self.last+=1
        return self.last


def parseGeneRecord(rec):
    """Parses a gene record of .gos fasta file."""
    locus = PATTERN_LOCUS.match(rec)

    if locus:
        locus = locus.groups()[0]
    else:
        locus = None
        #raise Exception('Invalid format while parsing gene record, ' + \
        #                'locus not found in "%s"' % rec)    
    
    chromosome = PATTERN_CHR.match(rec)
    if not chromosome:
        raise Exception('Invalid format while parsing gene record, ' + \
                        'chromosome not found in "%s"' % rec)
    chromosome = chromosome.groups()[0]

    strand = PATTERN_STRAND.match(rec)
    if not strand:
        raise Exception('Invalid format while parsing gene record, ' + \
                        'strand not found in "%s"' % rec)
    strand = strand.groups()[0]
    
    return {'id':rec, 'locus':locus, 'chr':chromosome, 'strand':strand}


def readGenomeGos(fname):
    """Reads one genome in .gos format given the file name. We suppose
    that fname ends with .gos. That file format stores the gene order
    sequence of a genome X in multi-record fasta format. The ID of
    each record is a concatenated string of key/value pairs delimited
    by |, the single most important being the chromosome|<chromosome
    ID> attribute. The succession of records corresponds to the genes'
    order in their corresponding gene order sequences.
    """

    genes = OrderedDict() # it's ordered (important!)
    order = 1
    
    with open(fname, 'r') as f:
        no_locus = False
        for l in f:
            if not l.startswith('>'):
                continue
            l = l[1:].strip() # remove >
            gene = parseGeneRecord(l)
            no_locus = no_locus or not gene['locus']
            gene['order'] = order
            genes[order] = gene
            order += 1

        if no_locus:
            print('Warning: Missing locus for one or more entries ' + \
                  'while parsing "%s"' % fname, file=stderr)
            
    return genes

def readGenomesMap(genome_map_file):
    """Reads the genome map file given the File object, and the .gos files
    of genomes in the genome map. For now, we assume just linear
    chromosomes, we have no such information stored in current file
    formats.
    """
    
    config = ConfigParser()
    config.read_file(genome_map_file)
    
    # NOTE: we need to map the order of the gene in active_genes (the
    # same number used by ffgc in .sim files) to the global order of
    # genes (the order in the .gos/fasta file, which defines the order
    # they appear in their chromosomes). That's why we have the
    # active2global entry.
    
    data = dict()
    letter_index = 0 # maximum of 26 genomes because of possible labels in ascii_uppercase
    for Gx in config.sections():
        gdata = { 'name':Gx, 'label':ascii_uppercase[letter_index] }
        letter_index += 1
        for k, v in config.items(Gx):
            if k == ACTIVE_GENES_KEY:
                active_order = { gid:order+1 for order, gid in enumerate(map(lambda x: x.strip(), v.split(','))) }
                v = set(active_order.keys())
            elif k == FASTA_FILE_KEY:
                k = 'genes'
                map_dir = path.dirname(genome_map_file.name) or '.'
                v = v if path.isabs(v) else map_dir + '/' + v
                v = readGenomeGos(path.normpath(v))
            gdata[k] = v

        required_keys = (ACTIVE_GENES_KEY, 'genes')
        for k in required_keys:
            if k not in gdata:
                raise Exception('Error while parsing genome map, ' + \
                                'key %s not found in section "%s" of' \
                                '"%s"' % (k, Gx, genome_map_file) )

        # active_genes defines a subset of genes that is in gene
        # similarity graph, but because we are working with all kind
        # of indels, we want to consider all genes in chromosomes, not
        # only "active" genes
        chromosomes = OrderedDict()
        active_genes = gdata[ACTIVE_GENES_KEY]
        active2global = dict()
        for gene in gdata['genes'].values():
            gid, gchr, gorder = gene['id'], gene['chr'], gene['order']
            gene['active'] = gene['id'] in active_genes
            if gene['active']:
                active2global[active_order[gid]] = gorder
            gene['edges'] = list()
            if gchr not in chromosomes:
                chromosomes[gchr] = { 'linear':True, 'circular':False, 'fake':False, 'genes':list() }
            chromosomes[gchr]['genes'].append(gorder)
        for c in chromosomes.values():
            c['genes'].sort()
        gdata['chromosomes'] = chromosomes
        gdata['active2global'] = active2global
        
        data[Gx] = gdata
        
    return data


def readAndTranslateSimFile(genome1, genome2, sim_file, setting, threshold, keep_chr_data = False):
    """Reads gene associations (edges) in a sim file and translates the
    gene orders from active gene order to global gene order. See
    readSimilaritiesFile + translateSimilarityEdges.
    """
    edges = readSimilaritiesFile(sim_file, setting, threshold, keep_chr_data)
    translateSimilarityEdges(edges, genome1, genome2)
    return edges


def translateSimilarityEdges(edges, genome1, genome2):

    """Translates the gene numbers in edges read from a similarity file,
    which is the order of genes in the list of **active genes**
    (active_genes in genome_map.cfg) to the order of all genes (as in the
    .gos/fasta files)."""
    for e in edges:
        e['g1'] = genome1['active2global'][e['g1']]
        e['g2'] = genome2['active2global'][e['g2']]


def readSimilaritiesFile(sim_file, setting, threshold, keep_chr_data = False):
    """Given the file name or File object, reads the file with computed
    gene similarities.  rel_orient is 1 if the strand of both genes is
    + or -, and -1 if the strands are different (+ and -, or - and +).
    All weights are set to 1 if setting == 'gc'. Edges with weights
    smaller than threshold are ignored.
    """
    edges = []
    keys = ('chr1', 'g1', 'chr2', 'g2', 'rel_orient', 'weight')
    #types = (str, int, str, int, int, float)
    weight_type = (lambda x: 1) if setting == UNW_FAMILY_FREE_ID else float
    if keep_chr_data:
        types = (str, int, str, int, int, weight_type)
    else:
        types = (None, int, None, int, int, weight_type) # not saving chromosomes

    if isinstance(sim_file, str):
        f = open(sim_file, 'r')
    else:
        f = sim_file
        
    for l in f:
        values = l.split()
        if float(values[-1]) < threshold: # float(values[-1]) => weight
            continue
        edges.append( { k: t(v) for k,v,t in zip(keys,values,types) if t is not None})
    return edges


def writeGeneNumberMap(genome, map_file):
    """Writes the gene number map. A genome map starts with ">genome_name"
    followed by lines in the format old_number new_number.
    """
    map_file.write(">%s\n" % genome['name'])
    for g in genome['genes'].values():
        map_file.write("%s %s\n" % (g['order'], g['unique_id']))
    

def readGeneNumberMap(map_file):
    """Reads the mapping of gene IDs and guesses the first and second
    genome names.
    """
    G1name = map_file.readline()[1:-1] # without starting > and ending \n
    G1map = dict()
    line = map_file.readline()
    while not line.startswith('>'):
        idorig, idnew = line.split()
        G1map[int(idorig)] = int(idnew)
        line = map_file.readline()
    G2name = line[1:-1]
    G2map = dict()
    for line in map_file: # remaining lines
        idorig, idnew = line.split()
        G2map[int(idorig)] = int(idnew)

    return { G1name:G1map, G2name:G2map }, G1name, G2name

def parseSolMatching(fsol):
    """Extract matching edges from a (cplex) solution file."""
    tree = ET.parse(fsol)
    root = tree.getroot()
    header = root.find('header')

    match = [] # matching edges

    # we round because "almost 0" sometimes happen, and that means 0
    allvars = ( ( var.get('name'), round(float(var.get('value'))) ) for vsection in root.findall('variables') for var in vsection )

    for name,value in allvars:
        if not name.startswith('x_') or value == 0:
            continue

        g1,g2 = name[3:-1].split(',')

        if g1[-1] not in ('h','t'): # cap
            continue

        num1,genome1,ext1 = g1.split('_') # gene number, genome and extremity type
        num2,genome2,ext2 = g2.split('_')

        if num1 == num2 or genome1 == genome2: # indel or adjacency edge
            continue

        match.append( (g1,g2) )

    return match

